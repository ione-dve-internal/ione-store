
(function(){
    // ========================== Requesting From Api ====================================
    function getHotProduct() {
        return new Promise(function (resolve, reject) {
            $.ajax({
                type: 'POST',
                url: Url('product_hot'),
                headers:{
                    'X-Parse-Application-Id':'_8Z6tG`B{s^NzjJv'
                }
            }).done(function(res){
                resolve(res);
            });
        });
    }

    function getRecommendedProduct() {
        return new Promise(function (resolve, reject) {
            $.ajax({
                type: 'POST',
                url: Url('product_recommend'),
                headers:{
                    'X-Parse-Application-Id':'_8Z6tG`B{s^NzjJv'
                }
            }).done(function(res){
                resolve(res);
            });
        });
    }
    //============================ End Request ========================================





    //  ===================== Calling To Get Recommended Product =============================
    getHotProduct().then(function (res) {
        // console.log(res.result);
        let result = res.result;


        $.each(result, function (index, item) {
            let img = 'default.png';
            if(item.images.length > 0)
                img = item.images[0];
            console.log(item.name);

            //set limited name for 20 charactors strings otherwise ...
            let limited_name = item.name;
            if(limited_name.length>20){
                limited_name = limited_name.substring(0,20) + ' . . .';
            }


            let link = "location.href='http://localhost:8000/product-hot'";
            let html = '<div class="col-sm-10 col-md-10 col-lg-2 card" style="border-radius:5px; margin:20px; cursor:pointer;" id="pcard" onclick="'+link+'"> ' +
                '           <div class="col-sm-12 col-md-12 col-lg-12 p-b-50 id="pcard1">' +
                '               <div class="block2 ">' +
                '                   <div class="block2-img wrap-pic-w of-hidden pos-relative">' +
                '                       <img src="'+img+'" alt="IMG-PRODUCT" class="img-fluid">' +
                '                       <div class="block2-overlay trans-0-4">' +
                '                           <a href="#" class="block2-btn-addwishlist hov-pointer trans-0-4">' +
                '                               <i class="icon-wishlist icon_heart_alt" aria-hidden="true"></i>' +
                '                               <i class="icon-wishlist icon_heart dis-none" aria-hidden="true"></i>' +
                '                           </a>' +
                '                           <div class="block2-btn-addcart w-size1 trans-0-4">' +
                '                               <!-- Button -->' +
                '                               <button class="flex-c-m size1 bg4 bo-rad-23 hov1 s-text1 trans-0-4">' +
                '                                   Add to Cart' +
                '                               </button>' +
                '                           </div>' +
                '                       </div>' +
                '                   </div>' +
                '                   <div class="block2-txt p-t-20">' +
                '                       <a href="#" class="block2-name dis-block s-text3 p-b-5">' +
                '                           '+limited_name+' ' +
                '                       </a>' +
                '                       <span class="block2-price m-text6 p-r-5" id="pcard4">'+'$ '+item.price+'</span>' +
                '                   </div>' +
                '               </div>' +
                '           </div>' +
                '       </div>';

            $('#section-hot-product').append(html);

        });
        //=================== End Loop Foreach ======================



        //=================== Click Get Array Object Index ===============
        $("#pcard, #pcard1").click(function(){
            var index = $("#pcard, #pcard1").index(this);
            console.log(index);
            sessionStorage.setItem("hot_product_index", index);// session for storing index number
            let p = result[index].price;console.log(p);

        });

    });
    //======================= End Calling Get Hot Product =====================================


    //  ===================== Calling To Get Recommended Product =============================
    getRecommendedProduct().then(function (res) {
        // console.log(res.result);
        let result = res.result;
        $.each(result, function (index, item) {
            let img = 'default.png';
            if(item.images.length > 0)
                img = item.images[0];

            let limited_name = item.name;
            if(limited_name.length>20){
                limited_name = limited_name.substring(0,20) + ' . . .';
            }

            let link = "location.href='http://localhost:8000/product-recommended'";
            let html = '<div class="col-sm-10 col-md-10 col-lg-2 card" style="border-radius:5px; margin:20px; cursor:pointer;" id="recommended" onclick="'+link+'"> ' +
                '           <div class="col-sm-12 col-md-12 col-lg-12 p-b-50 id="recommended1">' +
                '               <div class="block2 ">' +
                '                   <div class="block2-img wrap-pic-w of-hidden pos-relative">' +
                '                       <img src="'+img+'" alt="IMG-PRODUCT" class="img-fluid">' +
                '                       <div class="block2-overlay trans-0-4">' +
                '                           <a href="#" class="block2-btn-addwishlist hov-pointer trans-0-4">' +
                '                               <i class="icon-wishlist icon_heart_alt" aria-hidden="true"></i>' +
                '                               <i class="icon-wishlist icon_heart dis-none" aria-hidden="true"></i>' +
                '                           </a>' +
                '                           <div class="block2-btn-addcart w-size1 trans-0-4">' +
                '                               <!-- Button -->' +
                '                               <button class="flex-c-m size1 bg4 bo-rad-23 hov1 s-text1 trans-0-4">' +
                '                                   Add to Cart' +
                '                               </button>' +
                '                           </div>' +
                '                       </div>' +
                '                   </div>' +
                '                   <div class="block2-txt p-t-20">' +
                '                       <a href="#" class="block2-name dis-block s-text3 p-b-5">' +
                '                           '+limited_name+' ' +
                '                       </a>' +
                '                       <span class="block2-price m-text6 p-r-5" id="pcard4">'+'$ '+item.price+'</span>' +
                '                   </div>' +
                '               </div>' +
                '           </div>' +
                '       </div>';
            $('#section-recommended-product').append(html);
        });


        //=================== Click Get Array Object Index ===============
        $("#recommended, #recommended1").click(function(){
            var index = $("#recommended, #recommended1").index(this);
            console.log(index);
            sessionStorage.setItem("recommended_product_index", index);// session for storing index number
            let p = result[index].price;console.log(p);
        });
    });

})();
