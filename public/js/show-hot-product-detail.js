
(function(){
    function getHotProduct(callback) {

            $.ajax({
                type: 'POST',
                url: 'http://ione-app-api.ioneservice.com/parse/functions/product_hot',
                headers:{
                    'X-Parse-Application-Id':'_8Z6tG`B{s^NzjJv'
                },
                async: false
            }).success(function(res){
                callback(res);
            });
    }


    $('#product-cart').text(sessionStorage.getItem("test"));



    getHotProduct(function (res) {
        // console.log(res.result);
        let result = res.result;
        let productCode, productName, productPrice, productDesc, productImg;

        let productIndex = sessionStorage.getItem("hot_product_index");

        productCode = result[productIndex].code;
        productName = result[productIndex].name;
        productPrice = result[productIndex].price;
        productDesc = result[productIndex].desc;
        productImg = result[productIndex].images;

        productDesc=productDesc.split(".").join(".<br><br>");

        let hoverButton =" this.style.backgroundColor='#4c78ba'; ";
        let outButton =" this.style.backgroundColor='black'; ";
        let html = '<div class="col-lg-5">'+
            '   <div class="card" style="width:400px">'+
            '   <img class="card-img-top" src="'+productImg[0]+'" data-zoom-image="'+productImg[0]+'" alt="Card image" style="width:100%" id="zoom">'+
            '       <div class="card-body">'+
            '           <hr><p class="card-text">'+productName+'</p>'+
            '       </div>'+
            '   </div>'+
            '</div>'+
            '<div class="col-lg-7 ">'+
            '   <h3 id="productName">'+productName+'</h3>'+
            '   <p style="font-size:30px;"><b id="productPrice">'+ '$'+productPrice+'<b></p> <hr>'+
            '   <div class="input-group">'+
            '       <div class="input-group" style="width:100%;">' +
            '           <span class="input-group-btn">' +
            '               <button type="button" class="quantity-left-minus btn btn-danger btn-number"  data-type="minus" data-field="">' +
            '                   <span class="fa fa-minus"></span>' +
            '               </button>' +
            '           </span>' +
            '           <input type="text" id="quantity" name="quantity" class="form-controll input-number" value="1" min="1" max="100" style="background-color:#adafb2; color:white; text-align:center;">' +
            '           <span class="input-group-btn">' +
            '               <button type="button" class="quantity-right-plus btn btn-success btn-number" data-type="plus" data-field="">' +
            '                   <span class="fa fa-plus"></span>' +
            '               </button>' +
            '           </span>' +
            '       </div>'+
            '       <div class="input-group"> <button class="btn" style="background-color:black; color:white;" onmouseover="'+hoverButton+'" onmouseout="'+outButton+'" id="btnAddToCart">Add to cart</button></div>'+
            '   </div>'+
            '   <hr> '+
            '   <ul class="nav nav-tabs" role="tablist">' +
            '       <li class="nav-item">' +
            '           <a class="nav-link active" href="#description" role="tab" data-toggle="tab">Description</a>' +
            '       </li>' +
            '       <li class="nav-item">' +
            '           <a class="nav-link " href="#review" role="tab" data-toggle="tab">Reviews</a>' +
            '       </li>' +
            '   </ul>' +
            '   <!-- Tab panes -->' +
            '   <div class="tab-content">' +
            '       <div role="tabpanel" class="tab-pane fade in active show" id="description">'+
            '         <h5 style="margin-top:20px;">Product Description</h5> <p style="padding-top:20px; line-height: 250%;" id="pdesc">'+productDesc+'</p><hr> '+
            '         <h5 style="margin-top:20px;">Product Code</h5> <p style="padding-top:20px;">Product Code : '+productCode+'</p> <hr>'+
            '       </div>' +
            '       <div role="tabpanel" class="tab-pane fade " id="review" >'+
            '         <h5 style="margin-top:20px;">Product Review</h5> <p style="padding-top:20px; line-height: 200%;">There is no review yet.</p> '+
            '           <h6 style="padding-top:20px; line-height: 200%;">Be the first to review   \"'+productName+'\"  </h6>'+
            '           <small style="padding-top:20px; line-height: 200%;">You must be logged in to post a review.</small><hr>'
        '       </div>' +
        '   </div>'
        '</div>';



        $("#show_product").append(html);


        //======= zoom image ======
        $('#zoom').elevateZoom({
            scrollZoom: true
        });
        //======== end zoom image ========

        //============== Working on clicking increasing and descreasing quantitiy=============
        var quantitiy=0;
        $('.quantity-right-plus').click(function(e){

            // Stop acting like a button
            e.preventDefault();
            // Get the field name
            var quantity = parseInt($('#quantity').val());

            // If is not undefined

            $('#quantity').val(quantity + 1);


            // Increment

        });

        $('.quantity-left-minus').click(function(e){
            // Stop acting like a button
            e.preventDefault();
            // Get the field name
            var quantity = parseInt($('#quantity').val());

            // If is not undefined

            // Increment
            if(quantity>1){
                $('#quantity').val(quantity - 1);
            }
        });
        //================ End clicking increasing and descreasing ===================



        //=============== click add to cart button ==================
        $("#displayNumberItem").text(localStorage.getItem("numerInCartFromLayout"));
        let numberInCart = parseInt($('#displayNumberItem').text());

        $('#btnAddToCart').click(function(){
            numberInCart+=1;
            $('#displayNumberItem').text(numberInCart);
            localStorage.setItem("numberInCart", numberInCart);
            // ================ end increase number in cart ===================




            //========= store product in cart when click ===================
            let session = localStorage.getItem("productListInCart");

            if(session!==null){
                let str = session.toString();
                let arr = str.split(",");
                console.log(typeof arr);
                console.log(arr);

                arr.push(productIndex);
                localStorage.setItem("productListInCart", arr);
                console.log(arr);

            }else{
                localStorage.setItem("productListInCart", productIndex);
                console.log(productIndex);
            }
            // ========= end store product in cart ================

        });
        // ================= end click add to cart button ===========


        // ================== click on cart image ===================
        $("#cart-img").click(function(){

            let indexes = localStorage.getItem("productListInCart");
            if(indexes!==null){
                console.log(typeof indexes);
                let arrayIndex = indexes.split(",");
                console.log(arrayIndex);

                let qty = 0;
                $('#header-cart-wrapitem').html('');
                for(let i=0; i<arrayIndex.length; i++) {
                    // console.log(arrayIndex[i]);
                    console.log(result[arrayIndex[i]].name);
                    qty = $("#quantity").val();
                    localStorage.setItem("product_qty", qty);  /// must plus porudct index to make unique storage otherwise the qty will be the same ........
                    let cartItem = $('#header-cart-wrapitem');

                    let item = '<li class="header-cart-item">' +
                        '           <div class="header-cart-item-img">' +
                        '               <img src="' + result[arrayIndex[i]].images[0] + '" alt="IMG" >' +
                        '           </div>' +
                        '           <div class="header-cart-item-txt">' +
                        '               <a href="#" class="header-cart-item-name">' +
                        '                   ' + result[arrayIndex[i]].name +
                        '               </a>' +
                        '               <span class="header-cart-item-info">' +
                        '               ' +localStorage.getItem("product_qty")+ ' x $'+result[arrayIndex[i]].price +
                        '               </span>' +
                        '           </div>' +
                        '       </li>';
                    cartItem.append(item);
                }
            }else{
                return "";
            }
            // $("#cart-img").css('pointer-events', 'none');
        });
        // ========== end click on cart image =============================



    });

})();

