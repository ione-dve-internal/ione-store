(function(){


    function  getRecommendedProduct() {
        return new Promise(function (resolve, reject) {
            $.ajax({
                type: 'POST',
                url: 'http://ione-app-api.ioneservice.com/parse/functions/product_recommend',
                headers:{
                    'X-Parse-Application-Id':'_8Z6tG`B{s^NzjJv'
                },
                async: false
            }).success(function(res){
                resolve(res);
            });
        });
    }


    console.log(sessionStorage.getItem("recommended_product_index"));




    getRecommendedProduct().then(function (res) {
        // console.log(res.result);
        let result = res.result;
        let productCode, productName, productPrice, productDesc, productImg;

        let productIndex = sessionStorage.getItem("recommended_product_index");

        productCode = result[productIndex].code;
        productName = result[productIndex].name;
        productPrice = result[productIndex].price;
        productDesc = result[productIndex].desc;
        productImg = result[productIndex].images;

        productDesc=productDesc.split(".").join(".<br><br>");




        let hoverButton =" this.style.backgroundColor='#4c78ba'; ";
        let outButton =" this.style.backgroundColor='black'; ";
        let html = '<div class="col-lg-5">'+
            '   <div class="card" style="width:400px">'+
            '   <img id="zoom" class="card-img-top "  src="'+productImg[0]+'" data-zoom-image="'+productImg[0]+'" alt="Card image" style="width:100%" >'+
            '       <div class="card-body">'+
            '           <p class="card-text"><hr>'+productName+'</p>'+
            '       </div>'+
            '   </div>'+
            '</div>'+
            '<div class="col-lg-7 ">'+
            '   <h3>'+productName+'</h3>'+
            '   <p style="font-size:30px;"><b>'+ '$'+productPrice+'<b></p> <hr>'+
            '   <div class="input-group">'+
            '       <div class="input-group" style="width:100%;">' +
            '           <span class="input-group-btn">' +
            '               <button type="button" class="quantity-left-minus btn btn-danger btn-number"  data-type="minus" data-field="">' +
            '                   <span class="fa fa-minus"></span>' +
            '               </button>' +
            '           </span>' +
            '           <input type="text" id="quantity" name="quantity" class="form-controll input-number" value="1" min="1" max="100" style="background-color:#adafb2; color:white; text-align:center;">' +
            '           <span class="input-group-btn">' +
            '               <button type="button" class="quantity-right-plus btn btn-success btn-number" data-type="plus" data-field="">' +
            '                   <span class="fa fa-plus"></span>' +
            '               </button>' +
            '           </span>' +
            '       </div>'+
            '       <div class="input-group" > <button class="btn" style="background-color:black; color:white;" onmouseover="'+hoverButton+'" onmouseout="'+outButton+'">Add to cart</button></div>'+
            '   </div>'+
            '   <hr> '+
            '   <ul class="nav nav-tabs" role="tablist">' +
            '       <li class="nav-item">' +
            '           <a class="nav-link active" href="#description" role="tab" data-toggle="tab">Description</a>' +
            '       </li>' +
            '       <li class="nav-item">' +
            '           <a class="nav-link " href="#review" role="tab" data-toggle="tab">Reviews</a>' +
            '       </li>' +
            '   </ul>' +
            '   <!-- Tab panes -->' +
            '   <div class="tab-content">' +
            '       <div role="tabpanel" class="tab-pane fade in active show" id="description">'+
            '         <h5 style="margin-top:20px;">Product Description</h5> <p style="padding-top:20px; line-height: 250%;" id="pdesc">'+productDesc+'</p><hr> '+
            '         <h5 style="margin-top:20px;">Product Code</h5> <p style="padding-top:20px;">Product Code : '+productCode+'</p> <hr>'+
            '       </div>' +
            '       <div role="tabpanel" class="tab-pane fade " id="review" >'+
            '         <h5 style="margin-top:20px;">Product Review</h5> <p style="padding-top:20px; line-height: 200%;">There is no review yet.</p> '+
            '           <h6 style="padding-top:20px; line-height: 200%;">Be the first to review   \"'+productName+'\"  </h6>'+
            '           <small style="padding-top:20px; line-height: 200%;">You must be logged in to post a review.</small><hr>'
        '       </div>' +
        '   </div>'
        '</div>';


        $("#show_product").append(html);

        //==== zoom image jquery plugin ====
        $('#zoom').elevateZoom({
            scrollZoom: true
        });
        //=== end zoom image ==============
        $('#zoom').mouseover(function (){
            $('#plus_sign').hide();
        });

        //============== Working on clicking increasing and descreasing quantitiy=============
        var quantitiy=0;
        $('.quantity-right-plus').click(function(e){

            // Stop acting like a button
            e.preventDefault();
            // Get the field name
            var quantity = parseInt($('#quantity').val());

            // If is not undefined

            $('#quantity').val(quantity + 1);


            // Increment

        });

        $('.quantity-left-minus').click(function(e){
            // Stop acting like a button
            e.preventDefault();
            // Get the field name
            var quantity = parseInt($('#quantity').val());

            // If is not undefined

            // Increment
            if(quantity>0){
                $('#quantity').val(quantity - 1);
            }
        });
        //================ End clicking increasing and descreasing ===================
    });

})();
