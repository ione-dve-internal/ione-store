@extends('front-end.partials.layout')
@section('active-page-home', 'sale-noti')
@section('content')
    <!-- ==================== SECTION ================= -->
    <!-- Slide1 -->
    @include('front-end.sections.slide1-section')
    <!-- End Slide1 -->
    <!-- Hot product -->
    @include('front-end.sections.hot-product-section')
    <!-- End product -->


    <!-- Recommended Product -->
    @include('front-end.sections.recommended-product-section')
    <!-- End Recommended Product -->

    <!-- branch -->
    @include('front-end.sections.our-branch-section')
    <!-- End branch -->

    <!-- search banner -->
    @include('front-end.sections.search-banner')
    <!-- End search banner -->
    <!-- =============== END SECTION =============== -->

@endsection