
<footer class="bg6 p-t-45 p-b-43 p-l-45 p-r-45" style="background-color:#333435;">
    <div class="flex-w p-b-90">
        <div class="w-size6 p-t-30 p-l-15 p-r-15 respon3">
            <h4 class="s-text12 p-b-30" style="color:white;">
                GET IN TOUCH
            </h4>

            <div>
                <p class="s-text8 w-size27">
                    Nº. 25, Mao Tse Tung Blvd., 1st Floor, iOne Building,
                    Boeung Keng Kang 1, Chamkar Morn, 12302 Phnom Penh, Cambodia.(<i class="fa fa-phone"></i>+855 23 99 61 72)
                </p>

                <div class="flex-m p-t-30">
                    <a href="http://facebook.com/ione2ustore" target="_brank" class="fs-18 color1 p-r-20 fa fa-facebook"></a>
                    <a href="http://instagram.com/iOneCambodia" target="_brank" class="fs-18 color1 p-r-20 fa fa-instagram"></a>
                    <a href="#" class="fs-18 color1 p-r-20 fa fa-pinterest-p"></a>
                    <a href="#" class="fs-18 color1 p-r-20 fa fa-snapchat-ghost"></a>
                    <a href="http://youtube.com/iOneCambodia" target="_brank" class="fs-18 color1 p-r-20 fa fa-youtube-play"></a>
                </div>
            </div>
        </div>

        <div class="w-size7 p-t-30 p-l-15 p-r-15 respon4">
            <h4 class="s-text12 p-b-30" style="color:white;">
                Categories
            </h4>

            <ul style="color:white;">
                <li class="p-b-9">
                    <a href="#" class="s-text8">
                        App accessories
                    </a>
                </li>

                <li class="p-b-9">
                    <a href="#" class="s-text8">
                        Apple accessories
                    </a>
                </li>

                <li class="p-b-9">
                    <a href="#" class="s-text8">
                        Bags & Sleeves
                    </a>
                </li>

                <li class="p-b-9">
                    <a href="#" class="s-text8">
                        Cables & Adapters
                    </a>
                </li>
            </ul>
        </div>

        <div class="w-size7 p-t-30 p-l-15 p-r-15 respon4">
            <h4 class="s-text12 p-b-30" style="color:#e5e6e8;">
                Links
            </h4>

            <ul>
                <li class="p-b-9">
                    <a href="#" class="s-text8">
                        Search
                    </a>
                </li>

                <li class="p-b-9">
                    <a href="{{url('about')}}" class="s-text8">
                        About Us
                    </a>
                </li>

                <li class="p-b-9">
                    <a href="#" class="s-text8">
                        Contact
                    </a>
                </li>

                <li class="p-b-9">
                    <a href="#" class="s-text8">
                        Support
                    </a>
                </li>
            </ul>
        </div>

        <div class="w-size7 p-t-30 p-l-15 p-r-15 respon4">
            <h4 class="s-text12 p-b-30" style="color:#e5e6e8;">
                Help
            </h4>

            <ul>
                <li class="p-b-9">
                    <a href="#" class="s-text8">
                        Track Order
                    </a>
                </li>

                <li class="p-b-9">
                    <a href="#" class="s-text8">
                        Returns
                    </a>
                </li>

                <li class="p-b-9">
                    <a href="#" class="s-text8">
                        Shipping
                    </a>
                </li>

                <li class="p-b-9">
                    <a href="#" class="s-text8">
                        FAQs
                    </a>
                </li>
            </ul>
        </div>

        <div class="w-size8 p-t-30 p-l-15 p-r-15 respon3">
            <h4 class="s-text12 p-b-30" style="color:#e5e6e8;">
                Newsletter
            </h4>

            <form>
                <div class="effect1 w-size9">
                    <input class="s-text7 bg6 w-full p-b-5" type="text" name="email" placeholder="email@example.com">
                    <span class="effect1-line"></span>
                </div>

                <div class="w-size2 p-t-20">
                    <!-- Button -->
                    <button class="flex-c-m size2 bg4 bo-rad-23 hov1 m-text3 trans-0-4">
                        Subscribe
                    </button>
                </div>
            </form>
        </div>
    </div>

    <div class="t-center p-l-15 p-r-15">
        <a href="#">
            <img class="h-size2" src="{{asset('images/icons/paypal.png')}}" alt="IMG-PAYPAL">
        </a>

        <a href="#">
            <img class="h-size2" src="{{asset('images/icons/visa.png')}}" alt="IMG-VISA">
        </a>

        <a href="#">
            <img class="h-size2" src="{{asset('images/icons/mastercard.png')}}" alt="IMG-MASTERCARD">
        </a>

        <a href="#">
            <img class="h-size2" src="{{asset('images/icons/express.png')}}" alt="IMG-EXPRESS">
        </a>

        <a href="#">
            <img class="h-size2" src="{{asset('images/icons/discover.png')}}" alt="IMG-DISCOVER">
        </a>

        <div class="t-center s-text8 p-t-20">
            Copyright © 2019 iOne Co., Ltd., All rights reserved. | iOne Group <i class="fa fa-heart-o" aria-hidden="true"></i>
        </div>
    </div>
</footer>