<header class="header1">
    <!-- Header desktop -->
    <div class="container-menu-header">
        <div class="topbar">
            <div class="topbar-social">
                <a href="http://facebook.com/ione2ustore" target="_brank" class="topbar-social-item fa fa-facebook"></a>
                <a href="http://instagram.com/iOneCambodia" target="_brank" class="topbar-social-item fa fa-instagram"></a>
                <a href="#" class="topbar-social-item fa fa-pinterest-p"></a>
                <a href="#" class="topbar-social-item fa fa-snapchat-ghost"></a>
                <a href="http://youtube.com/iOneCambodia" target="_brank" class="topbar-social-item fa fa-youtube-play"></a>
            </div>

            <span class="topbar-child1">
					iOne2u Store  | First Mix-Brand Retail under iOne Group.
				</span>

            <div class="topbar-child2">
					<span class="topbar-email">
						iOne@example.com
					</span>

                <div class="topbar-language rs1-select2">
                    <select class="selection-1" name="time">
                        <option>USD</option>
                        <option>EUR</option>
                    </select>
                </div>
            </div>
        </div>

        <div class="wrap_header">
            <!-- Logo -->
            <a href="{{url('/')}}" class="logo">
                <img src="{{asset('images/icons/logo1.png')}}" alt="IMG-LOGO">
            </a>

            <!-- Menu -->
            <div class="wrap_menu">
                <nav class="menu">
                    <ul class="main_menu">
                        <li class=@yield('active-page-home')>
                            <a href="{{url('/')}}">Home</a>
                        </li>

                        <li class=@yield('active-page-about')>
                            <a href="{{url('about')}}">About Us</a>
                            <ul class="sub_menu">
                                <li class=@yield('active-page-about')><a href="{{url('about')}}">About Us</a></li>
                                <li><a href="{{url('outlets')}}">Outlets</a></li>
                                <li><a href="#">Latest</a></li>
                                <li><a href="{{url('career')}}">Career</a></li>
                            </ul>
                        </li>

                        <li  class=@yield('active-page-shopping')>
                            <a href="{{url('shoppings')}}">Shopping</a>
                        </li>

                        <li class=@yield('active-page-product')>
                            <a href="{{url('products')}}">Products</a>
                            <ul class="sub_menu">
                                <li><a href="#">App accessories</a></li>
                                <li><a href="#">Apple accessories</a></li>
                                <li><a href="#">Bags &Sleeves</a></li>
                                <li><a href="#">Cables &Adapters</a></li>
                                <li><a href="#">Cases &Protection</a></li>
                                <li><a href="#">Power banks</a></li>
                                <li><a href="#">Others</a></li>
                            </ul>
                        </li>

                        <li>
                            <a href="cart.html">Featured</a>
                            <ul class="sub_menu">
                                <li><a href="#">Instore</a></li>
                                <li><a href="#">Easy Payment</a></li>
                                <li><a href="#">Clearant</a></li>
                                <li><a href="#">Trad-in</a></li>
                            </ul>
                        </li>

                        <li class=@yield('active-page-support')>
                            <a href="#">Support</a>
                            <ul class="sub_menu">
                                <li><a href="#">O2O</a></li>
                                <li><a href="{{url('faqs')}}">FAQs</a></li>
                                <li><a href="{{url('t&c')}}">T &C</a></li>
                            </ul>
                        </li>

                        <li  class=@yield('active-page-contacts')>
                            <a href="{{url('contact')}}">Contact</a>
                            <ul class="sub_menu">
                                <li><a href="#">Social Media</a></li>
                                <li><a href="#">Directly</a></li>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>

            <!-- Header Icon -->
            <div class="header-icons">
                <a href="#" class="header-wrapicon1 dis-block">
                    <img src="{{asset('images/icons/icon-header-01.png')}}" class="header-icon1" alt="ICON">
                </a>

                <span class="linedivide1"></span>

                <div class="header-wrapicon2">
                    <img src="{{asset('images/icons/icon-header-02.png')}}" class="header-icon1 js-show-header-dropdown " alt="ICON" id="cart-img">
                    <span class="header-icons-noti" id="displayNumberItem">0</span>

                    <!-- Header cart noti -->
                    <div class="header-cart header-dropdown">
                        <ul class="header-cart-wrapitem" id="header-cart-wrapitem">
                            {{--<li class="header-cart-item">--}}
                                {{--<div class="header-cart-item-img">--}}
                                    {{--<img src="{{asset('images/item-cart-01.jpg')}}" alt="IMG">--}}
                                {{--</div>--}}

                                {{--<div class="header-cart-item-txt">--}}
                                    {{--<a href="#" class="header-cart-item-name">--}}
                                        {{--White Shirt With Pleat Detail Back--}}
                                    {{--</a>--}}

                                    {{--<span class="header-cart-item-info">--}}
											{{--1 x $19.00--}}
										{{--</span>--}}
                                {{--</div>--}}
                            {{--</li>--}}

                            {{--<li class="header-cart-item">--}}
                                {{--<div class="header-cart-item-img">--}}
                                    {{--<img src="{{asset('images/item-cart-02.jpg')}}" alt="IMG">--}}
                                {{--</div>--}}

                                {{--<div class="header-cart-item-txt">--}}
                                    {{--<a href="#" class="header-cart-item-name">--}}
                                        {{--Converse All Star Hi Black Canvas--}}
                                    {{--</a>--}}

                                    {{--<span class="header-cart-item-info">--}}
											{{--1 x $39.00--}}
										{{--</span>--}}
                                {{--</div>--}}
                            {{--</li>--}}

                            {{--<li class="header-cart-item">--}}
                                {{--<div class="header-cart-item-img">--}}
                                    {{--<img src="{{asset('images/item-cart-03.jpg')}}" alt="IMG">--}}
                                {{--</div>--}}

                                {{--<div class="header-cart-item-txt">--}}
                                    {{--<a href="#" class="header-cart-item-name">--}}
                                        {{--Nixon Porter Leather Watch In Tan--}}
                                    {{--</a>--}}

                                    {{--<span class="header-cart-item-info">--}}
											{{--1 x $17.00--}}
										{{--</span>--}}
                                {{--</div>--}}
                            {{--</li>--}}
                        </ul>

                        <div class="header-cart-total">
                            Total: $75.00
                        </div>

                        <div class="header-cart-buttons">
                            <div class="header-cart-wrapbtn">
                                <!-- Button -->
                                <a href="cart.html" class="flex-c-m size1 bg1 bo-rad-20 hov1 s-text1 trans-0-4">
                                    View Cart
                                </a>
                            </div>

                            <div class="header-cart-wrapbtn">
                                <!-- Button -->
                                <a href="#" class="flex-c-m size1 bg1 bo-rad-20 hov1 s-text1 trans-0-4" onclick="this.click(localStorage.clear(), window.location.reload())">
                                    Check Out
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Header Mobile -->
    <div class="wrap_header_mobile">
        <!-- Logo moblie -->
        <a href="{{url('/')}}" class="logo-mobile">
            <img src="{{asset('images/icons/logo1.png')}}" alt="IMG-LOGO">
        </a>

        <!-- Button show menu -->
        <div class="btn-show-menu">
            <!-- Header Icon mobile -->
            <div class="header-icons-mobile">
                <a href="#" class="header-wrapicon1 dis-block">
                    <img src="{{asset('images/icons/icon-header-01.png')}}" class="header-icon1" alt="ICON">
                </a>

                <span class="linedivide2"></span>

                <div class="header-wrapicon2">
                    <img src="{{asset('images/icons/icon-header-02.png')}}" class="header-icon1 js-show-header-dropdown" alt="ICON">
                    <span class="header-icons-noti">0</span>

                    <!-- Header cart noti -->
                    <div class="header-cart header-dropdown">
                        <ul class="header-cart-wrapitem">
                            <li class="header-cart-item">
                                <div class="header-cart-item-img">
                                    <img src="{{asset('images/item-cart-01.jpg')}}" alt="IMG">
                                </div>

                                <div class="header-cart-item-txt">
                                    <a href="#" class="header-cart-item-name">
                                        White Shirt With Pleat Detail Back
                                    </a>

                                    <span class="header-cart-item-info">
											1 x $19.00
										</span>
                                </div>
                            </li>

                            <li class="header-cart-item">
                                <div class="header-cart-item-img">
                                    <!--<img src="images/item-cart-02.jpg" alt="IMG">-->
                                </div>

                                <div class="header-cart-item-txt">
                                    <a href="#" class="header-cart-item-name">
                                        Converse All Star Hi Black Canvas
                                    </a>

                                    <span class="header-cart-item-info">
											1 x $39.00
										</span>
                                </div>
                            </li>

                            <li class="header-cart-item">
                                <div class="header-cart-item-img">
                                    <!--<img src="images/item-cart-03.jpg" alt="IMG">-->
                                </div>

                                <div class="header-cart-item-txt">
                                    <a href="#" class="header-cart-item-name">
                                        Nixon Porter Leather Watch In Tan
                                    </a>

                                    <span class="header-cart-item-info">
											1 x $17.00
										</span>
                                </div>
                            </li>
                        </ul>

                        <div class="header-cart-total">
                            Total: $75.00
                        </div>

                        <div class="header-cart-buttons">
                            <div class="header-cart-wrapbtn">
                                <!-- Button -->
                                <a href="cart.html" class="flex-c-m size1 bg1 bo-rad-20 hov1 s-text1 trans-0-4">
                                    View Cart
                                </a>
                            </div>

                            <div class="header-cart-wrapbtn">
                                <!-- Button -->
                                <a href="#" class="flex-c-m size1 bg1 bo-rad-20 hov1 s-text1 trans-0-4">
                                    Check Out
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="btn-show-menu-mobile hamburger hamburger--squeeze">
					<span class="hamburger-box">
						<span class="hamburger-inner"></span>
					</span>
            </div>
        </div>
    </div>

    <!-- Menu Mobile -->
    <div class="wrap-side-menu" >
        <nav class="side-menu">
            <ul class="main-menu">
                <li class="item-topbar-mobile p-l-20 p-t-8 p-b-8">
						<span class="topbar-child1">
							iOne2u Store  | First Mix-Brand Retail under iOne Group.
						</span>
                </li>

                <li class="item-topbar-mobile p-l-20 p-t-8 p-b-8">
                    <div class="topbar-child2-mobile">
							<span class="topbar-email">
								iOne@example.com
							</span>

                        <div class="topbar-language rs1-select2">
                            <select class="selection-1" name="time">
                                <option>USD</option>
                                <option>EUR</option>
                            </select>
                        </div>
                    </div>
                </li>

                <li class="item-topbar-mobile p-l-10">
                    <div class="topbar-social-mobile">
                        <a href="http://facebook.com/ione2ustore" target="_brank" class="topbar-social-item fa fa-facebook"></a>
                        <a href="http://instagram.com/iOneCambodia" target="_brank" class="topbar-social-item fa fa-instagram"></a>
                        <a href="#" class="topbar-social-item fa fa-pinterest-p"></a>
                        <a href="#" class="topbar-social-item fa fa-snapchat-ghost"></a>
                        <a href="http://youtube.com/iOneCambodia" target="_brank" class="topbar-social-item fa fa-youtube-play"></a>
                    </div>
                </li>

                <li class="item-menu-mobile">
                    <a href="index.html">Home</a>
                </li>

                <li class="item-menu-mobile">
                    <a href="{{url('about')}}">About Us</a>
                    <ul class="sub-menu">
                        <li><a href="{{url('outlets')}}">Outlets</a></li>
                        <li><a href="{{url('latest')}}">Latest</a></li>
                        <li><a href="{{url('career')}}">Career</a></li>

                    </ul>
                    <i class="arrow-main-menu fa fa-angle-right" aria-hidden="true"></i>
                </li>

                <li class="item-menu-mobile">
                    <a href="{{url('shoppings')}}">Shopping</a>
                </li>

                <li class="item-menu-mobile">
                    <a href="{{url('products')}}">Products</a>
                    <ul class="sub-menu">
                        <li><a href="#">App accessories</a></li>
                        <li><a href="#">Apple accessories</a></li>
                        <li><a href="#">Bags &Sleeves</a></li>
                        <li><a href="#">Cables &Adapters</a></li>
                        <li><a href="#">Cases &Protection</a></li>
                        <li><a href="#">Power banks</a></li>
                        <li><a href="#">Others</a></li>
                    </ul>
                    <i class="arrow-main-menu fa fa-angle-right" aria-hidden="true"></i>
                </li>

                <li class="item-menu-mobile">
                    <a href="blog.html">Featured</a>
                </li>

                <li class="item-menu-mobile">
                    <a href="about.html">Support</a>
                </li>

                <li class="item-menu-mobile">
                    <a href="{{url('contact')}}">Contact</a>
                </li>
            </ul>
        </nav>
    </div>
</header>