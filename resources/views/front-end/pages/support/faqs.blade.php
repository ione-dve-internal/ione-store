@extends('front-end.partials.layout')
@section('title', '- FAQs')
@section('active-page-support', 'sale-noti')
@section('link-bootstrap')
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <style>
        .accordion {
            /*background-color: #eee;*/
            color: #444;
            cursor: pointer;
            padding: 18px;
            width: 90%;
            border: none;
            text-align: left;
            outline: none;
            font-size: 15px;
            transition: 0.4s;
            font-weight:bold;
        }

        .active, .accordion:hover {
            color: #1074ba;
        }

        .accordion:after {
            content: '\002B';
            color: #777;
            font-weight: bold;
            float: left;
            margin-right: 20px;
        }

        .active:after {
            content: "\2212";
        }

        .panel {
            padding: 0 18px;
            background-color: white;
            max-height: 0;
            overflow: hidden;
            transition: max-height 0.2s ease-out;
        }

    </style>
@endsection

@section('content')
    <div class="container"><br><br><br>
        <h2>FAQs</h2><br>
        <p style="border-bottom: 2px solid whitesmoke;padding-top:15px;"></p>
        <button class="accordion" >What is iOne2u store ?</button>
        <div class="panel" style="border-bottom: 2px solid whitesmoke;padding-top:15px;">
            <p>I am text block. Click edit button to change this text. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.</p>
        </div>

        <button class="accordion">Why did you know iOne2ustore ?</button>
        <div class="panel"  style="border-bottom: 2px solid whitesmoke;padding-top:15px;">
            <p>I am text block. Click edit button to change this text. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.</p>
        </div>

        <button class="accordion">How do you pick up your product ?</button>
        <div class="panel"  style="border-bottom: 2px solid whitesmoke;padding-top:15px;">
            <p>I am text block. Click edit button to change this text. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.</p>
        </div>
        <button class="accordion">Where is iOne2ustore ?</button>
        <div class="panel"  style="border-bottom: 2px solid whitesmoke;padding-top:15px;">
            <p>I am text block. Click edit button to change this text. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.</p>
        </div><br><br><br><br><br>
    </div>
    <script>
        var acc = document.getElementsByClassName("accordion");
        var i;

        for (i = 0; i < acc.length; i++) {
            acc[i].addEventListener("click", function() {
                this.classList.toggle("active");
                var panel = this.nextElementSibling;
                if (panel.style.maxHeight){
                    panel.style.maxHeight = null;
                } else {
                    panel.style.maxHeight = panel.scrollHeight + "px";
                }
            });
        }
    </script>

@endsection