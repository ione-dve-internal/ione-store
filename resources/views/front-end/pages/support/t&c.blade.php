@extends('front-end.partials.layout')
@section('title', '- T&C')
@section('active-page-support', 'sale-noti')
@section('link-bootstrap')

    <style>
        .font{
            font-size:7px;
            padding-right:10px;
        }
        .outlets-fb{
            margin-top:18px;
            color:white;

        }
        .fb{
            background-color: #1cbac8;
            height:80px;
            text-align:center;
        }
    </style>
@endsection

@section('content')
    <!-- Title Page -->
    <div class="container">
        <h2 style="padding-top:100px;"><i class="fa fa-life-bouy" style="color:#1cbac8;  padding-right:10px;font-size:50px;"></i>Term & Condition </h2><br>

        <p>Welcome to iOne2u.com, the trendy online retail shopping of well-known brands for ICT (Information, Communication & Technology) products in Cambodia. To ensure a safe, enjoyable environment for all of our users, we have established our Terms of Use. They spell out what you can expect from us and what we expect from you. Please note that by accessing any area of iOne2u.com, you agree to be bound and to abide by these Terms of Use. We also strongly advise you to see our full privacy policy statement. iOne2u.com is owned and operated by iOne Co., Ltd.</p><br>

        <h4>Compliancec With Laws</h4><br>

        <p>iOne Co., Ltd is a company under the laws of Cambodia. We operate in Cambodia and comply with all the laws and regulations of Cambodia. All our products are in compliance with all the requirements and specifications under the laws of Cambodia. We are not in a position to guarantee compliance with specific local requirements or specifications under the local jurisdictions of customers. We recommend you to pickup product at iOne Service Center; you may contact us or make your own arrangement. Our products are denominated in US dollars with local currency equivalent for reference. All orders placed are transacted in US dollars.</p><br>

        <h4>No Shipping</h4><br>


        <p>In case you choose to buy product from iOne2u.com, your order can collect at iOne Service Center. In case you have a problem with your ordering which is different from our own suggestion, please contact our Customer Service Representative by phone 085 99 75 75 or 081 99 75 75 during office hours (08:30am to 12:00pm & 01:00pm to 05:00pm, Monday to Saturday) or by e-mail: <a href="mailto:information@ione2u.com"> information@ione2u.com</a></p><br>

        <h4>Our trademarks, service marks and copyright</h4><br>


        <p>The following trademarks and service marks belong to iOne Co., Ltd. or its affiliates and all rights in them are reserved: iOne. These and other iOne or iOne2u.com graphics, logos, service marks and trademarks of iOne Co., Ltd. All other trademarks, product names, company names and logos appearing on iOne2u.com are the property of their respective owners. All content on our site, including design, text, graphics, logos, button icons, images, software and audio clips; any improvements or modifications to such content; any derivative works thereof; the collection, arrangement and assembly of all content appearing on iOne2u.com, are the exclusive property of iOne Co., Ltd and are protected by Cambodia and international copyright and other applicable intellectual property laws.</p><br>

        <h4>Customer Permitted for Commerce</h4><br>


        <p>All products sold are for customers’ use. iOne2u.com may refuse any purchasing order which it suspects for any wrong purpose.</p><br>

        <h4>Disclaimer</h4><br>


        <p>The site and its content are provided on an “as is” basis without warranties of any kind, whether express or implied (except only as may be implied by applicable law). iOne2u.com is only providing a venue for the sale of products on its site and shall not be liable for any direct, indirect, incidental, special, consequential or exemplary damages (including damages for loss of profit or intangible loss related to your use of the site or use of any of the products purchased at its site). Please read all information, warranties or warnings provided by the manufacturer owner or seller of the products on or in the product packaging and labels before using any product purchased on iOne2u.com. iOne2u.com shall not be liable for the accuracy, fitness for a particular purpose or quality of such products or reliability of any information provided by any manufacturer, owner or seller of such products and it is your responsibility to evaluate such information.</p><br>

        <h4>Indemnity and Limitation of Liavility</h4><br>


        <p>You agree to indemnify, defend and hold harmless iOne2u.com from and against all losses, expenses, damages and costs (including reasonable legal fees) arising from any breach of these Terms of Use or any other activity by you in connection with your use of the site. You further agree that iOne2u.com shall not be held liable howsoever for your use of the site and you agree to waive all claims that you may have against iOne2u.com for any loss or damages you may sustain in connection with your use of the site. In the event that this disclaimer is unenforceable, the total liability of iOne2u.com, if any, for losses or damages shall not exceed the fees you paid for the particular service or product upon which liability is based.</p><br>


        <h4>Link to orther sites</h4><br>

        <p>Occasionally, iOne2u.com may make available a link to a third party’s web site. These links will let you leave iOne2u.com. The linked sites are not under iOne2u.com control and iOne2u.com is not responsible for the contents of any linked site or any link contained in a linked site, or any changes or updates to such sites. iOne2u.com is not responsible for webcasting or any other form of transmission received from any linked site. iOne2u.com provides the links only as a convenience. iOne2u.com does not endorse the site or its use or contents.</p><br>


        <h4>Our Privacy Policy</h4><br>
        <p>You agree to our use of information about you as described in our privacy policy.</p><br>

        <h4>Termination</h4><br>
        <p>iOne2u.com may terminate or suspend your access to all or part of the site, without notice, for any conduct that iOne2u.com, in its sole discretion, believes is in violation of any applicable law or is harmful to iOne2u.com or a third party..</p><br>

        <h4>Notification</h4><br>
        <p>iOne2u.com may deliver notices to you by means of electronic mail, a general notice on iOne2u.com or iOne Service Center. You may give notice to iOne2u.com at any time by means of electronic mail to iOne2u.com or come to iOne Service Center.</p><br>

    </div><br>
    <div class="fb">
        <div class="container">
            <div class="row">
                <div class="col-md-12 outlets-fb"style="background-color: #1cbac8;">
                    <span style="margin-right:40px;"> GET IN TOUCH WITH IONE</span>
                    <div class="btn text-center" style=" border: 2px solid gray;" onmouseover="this.style.border='2px solid white'" onmouseout="this.style.border='2px solid gray'">
                        <a href="https://www.facebook.com/iOne2uStore/" target="_blank" style="text-decoration:none; color:white;">
                            <i class="fa fa-address-card" style="padding-right:10px;font-size:20px; color:white;"> </i><span>Contacts Us</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection