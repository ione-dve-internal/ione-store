@extends('front-end.partials.layout')
@section('title', '- Product-Details')
@section('active-page-product', 'sale-noti')
@section('js')
    <script src="{{url('js/jquery.min.js')}}"></script>
    <script src="{{url('vendor/elevatezoom-master/jquery.elevatezoom.js')}}"></script>
    <script src="{{url('js/show-hot-product-detail.js')}}"></script>
    {{--<script src="{{url('js/click-on-cart.js')}}"></script>--}}
@endsection

@section('content')
    <section>
        <div class="container">
            <input type="text">
            <div class="row" id="show_product">

            </div>
        </div>
    </section>
@endsection