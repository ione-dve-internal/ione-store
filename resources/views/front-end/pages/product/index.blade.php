@extends('front-end.partials.layout')
@section('title', '- Products')

@section('active-page-product', 'sale-noti')
@section('content')
    @include('front-end.sections.hot-product-section')
    <!-- End Banner -->

    <!-- New Product -->
    @include('front-end.sections.recommended-product-section')
    <!-- End New Product -->

    <!-- Banner2 -->
    @include('front-end.sections.banner2-section')
    <!-- End Banner2 -->

    <!-- Blog -->
    @include('front-end.sections.our-outlet-section')
    <!-- End Blog -->

    <!-- Instagram -->
    @include('front-end.sections.our-branch-section')
    <!-- End Instagram -->

    <!-- Shipping -->
    @include('front-end.sections.shopping-section')
    <!-- End Shopping -->
@endsection

