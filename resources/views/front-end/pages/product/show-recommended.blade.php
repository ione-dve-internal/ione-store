@extends('front-end.partials.layout')
@section('title', '- Product-Details')
@section('active-page-product', 'sale-noti')
@section('js')
    <script src="{{url('js/jquery.min.js')}}"></script>
    <script src="{{url('vendor/elevatezoom-master/jquery.elevatezoom.js')}}"></script>
    <script src="{{url('js/show-recommended-product-detail.js')}}"></script>
@endsection

@section('content')
    <section>
        <div class="container">
            <div class="row" id="show_product">

            </div>
        </div>
    </section>
@endsection