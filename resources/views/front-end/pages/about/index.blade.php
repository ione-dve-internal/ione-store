@extends('front-end.partials.layout')
@section('title', '- About Us')
@section('active-page-about', 'sale-noti')
@section('link-bootstrap')
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
@endsection
@section('content')

    <!-- Title Page -->
    <section class="bg-title-page p-t-40 p-b-50 flex-col-c-m" style="background-image: url(images/heading-pages-07.jpg); height:80%">
        <!--<h2 class="l-text2 t-center">-->
        <!--	About-->
        <!--</h2>-->
    </section>
    <!-- content page -->
    <section class="bgwhite p-t-66 p-b-38">
        <div class="container">
            <div class="row">
                <div class="col-md-4 p-b-30">
                    <div class="hov-img-zoom">
                        <img src="images/about-2.png" alt="IMG-ABOUT">
                    </div>
                </div>

                <div class="col-md-8 p-b-30">
                    <h3 class="m-text26 p-t-15 p-b-16">
                        About Us
                    </h3>

                    <p class="p-b-20">
                        <span>iOne2u Store is the first Mix-Brand Retail Store under iOne Group.</span>

                    </p>
                    <p class="p-b-20">
                        Our store philosophy is simple, at iOne2u Store we get our inspiration from the best products around the world and deliver to our customers with the promise of guarantee and trust at the best value possible. Our varied products line share one common feature, that is they fulfil a lifestyle need with the best technology can offer.
                        Get curious, get inspired and discover how you can improve your modern lifestyle…. only at iOne2u Store… Discover your next lifestyle.
                    </p>
                    <div class="bo13 p-l-29 m-l-9 p-b-10">
                        <p class="p-b-11">
                            The store is your discovery centre for innovation gadgets, trendy wearable technology including practical lifestyle appliances and more. From drones to fruit blenders we carry only the best!
                        </p>

                        <span class="s-text7">
						<a href="http://www.ione2ustore.com">www.iOne2ustore.com</a>
						</span>
                    </div>
                </div>
            </div>
        </div>
    </section>


@endsection