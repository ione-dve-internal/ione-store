@extends('front-end.partials.layout')
@section('title', '- About Us')
@section('active-page-about', 'sale-noti')
@section('link-bootstrap')
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <style>
        .line{
            width: 60px;
            height: 30px;
            border-bottom: 3px solid #1074ba;
            position: relative;
        }
        .outlets-fb{
            margin-top:18px;
            color:white;

        }
        .fb{
            background-color: #3b5998;
            height:80px;
            text-align:center;
        }

    </style>

@endsection

@section('content')
    <div class="container">
        <i class="fa fa-home"style="font-size:50px;color:#1cbac8; padding-top:100px;"></i>
        <span style="color:black;font-size:40px;"> Stores</span>
        <br>
        <div class="row">
            <div class="col-md-7">
                <p>
                    The store is your discovery centre for innovation gadgets, trendy wearable technology including practical lifestyle appliances and more. From drones to fruit blenders we carry only the best!
                </p>
            </div>
            <div class="col-md-5 ">
                <div class="p-40" style=" background-color:#1074ba; text-align:center; padding:40px 40px 40px 40px; margin-left:79px;">
                    <p style="color:white;"><small>All iOne2 Stores are open</small>&nbsp;<strong>Monday to Sunday</strong>&nbsp;<small>from</small> <strong>8:00 AM to 8:00 PM</strong>.</p>
                </div>
            </div>
        </div>
    </div>
    <br><br><br><br>
    <div style=" background-color: #f9f9f9;  ">
        <div class="container">
            <div class="row">
                <div class="col-md-6 "><br><br>
                    <h4> iOne2u Store</h4>
                    <div class="line"></div><br>
                    <p> Nº. 25, Mao Tse Tung Blvd., 1st Floor, iOne Building,
                        Boeung Keng Kang 1, Chamkar Morn, 12302 Phnom Penh, Cambodia.
                    </p>
                    <p><i class="fa fa-phone"></i> +855 23 99 61 72</p>
                    <p><i class="fa fa-phone"></i> information@ione2u.com</p>
                </div>
                <div class="col-md-6 ">
                    <img src="{{asset('images/outlets-1.png')}}" class="img-fluid">
                </div>
            </div>
        </div>
    </div><br><br><br><br>
    <div style=" background-color: #f9f9f9;" >
        <div class="container">
            <div class="row ">
                <div class="col-md-6" ><br><br>
                    <h4>iOne2u Watch Aeon Mall</h4>
                    <div class="line"></div><br>
                    <p>AEON Mall Sen Sok City, St. 1003, Sangkat Phnom Penh Thmey, Khan Sen Sok, 12101 Phnom Penh</p>
                    <p><i class="fa fa-phone"></i> 023 99 61 72</p>
                    <p><i class="fa fa-phone"></i> information@ione2u.com</p>
                </div>
                <div class="col-md-6 ">
                    <img src="{{asset('images/outlets-1.png')}}" class="img-fluid">
                </div>
            </div>
        </div>
    </div><br><br><br><br>
    <div class="fb">
        <div class="container">
            <div class="row">
                <div class="col-md-12 outlets-fb">
                    <span style="margin-right:40px;"> FOLLOW US ON FACEBOOK</span>
                    <div class="btn text-center" style=" border: 2px solid gray;" onmouseover="this.style.border='2px solid white'" onmouseout="this.style.border='2px solid gray'">
                        <a href="https://www.facebook.com/iOne2uStore/" target="_blank" style="text-decoration:none; color:white;">
                            <i class="fa fa-facebook-square" style="padding-right:10px;font-size:20px; color:white;"> </i><span>IONE2USTORE</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection