@extends('front-end.partials.layout')
@section('title', '- About Us')
@section('active-page-about', 'sale-noti')
@section('link-bootstrap')

    <style>
        .font{
            font-size:7px;
            padding-right:10px;
        }
        .outlets-fb{
            margin-top:18px;
            color:white;

        }
        .fb{
            background-color: #1cbac8;
            height:80px;
            text-align:center;
        }
    </style>
@endsection

@section('content')
    <!-- Title Page -->
    <div class="container">
        <h2 style="padding-top:100px;"><i class="fa fa-life-bouy" style="color:#1cbac8;  padding-right:10px;font-size:50px;"></i>Careers at iOne</h2><br>

        <p>iOne Co., Ltd is the Authorized Distributor of well-known brands for ICT (Information, Communication & Technology) products in Cambodia. We are expanding our team and are seeking urgently dynamic candidates as below:</p><br>

        <h4>1. Marketing Coordinator (Handle Distribution Channel)</h4><br>

        <h4>Job Responsibilities</h4><br>

        <p><i class="fa fa-square font"></i>Coordinates and implements marketing communication projects with responsibilities that include public relations, special events management, advertising and creating brand awareness.</p>
        <p><i class="fa fa-square font"></i>Prepare product training.</p>
        <p><i class="fa fa-square font"></i>Handle social networking and social media.</p>
        <p><i class="fa fa-square font"></i>Typically reports to a supervisor or manager.</p>
        <p><i class="fa fa-square font"></i>Performs a variety of tasks.</p><br>

        <h4>Job Requirements</h4><br>

        <p><i class="fa fa-square font"></i>At least Associate Degree in Marketing, Business Administrative or equivalent academic qualification.</p>
        <p><i class="fa fa-square font"></i>At least one year of experience in the field or in a related area.</p>
        <p><i class="fa fa-square font"></i>Works under general supervision.</p>
        <p><i class="fa fa-square font"></i>A certain degree of creativity and latitude is required.</p>
        <p><i class="fa fa-square font"></i>Having excellent communication skills with all kinds of people in both spoken and written English.</p>
        <p><i class="fa fa-square font"></i>Good computer literacy (Internet, email, Microsoft office)</p><br>

        <h4>2. Showroom Assistant (3 Positions)</h4><br>

        <h4>Job Responsibilities</h4><br>

        <p><i class="fa fa-square font"></i>Greet customers as they arrive at the showroom and inquire into their purpose of visit</p>
        <p><i class="fa fa-square font"></i>Listen actively to customers while they provide details of what they want to buy or look at</p>
        <p><i class="fa fa-square font"></i>Assist customers in locating items of their choice and provide them with information on its features</p>
        <p><i class="fa fa-square font"></i>Assist customers in locating items of their choice and provide them with information on its features</p>
        <p><i class="fa fa-square font"></i>Demonstrate product features, by clearly articulating each important article and its use</p>
        <p><i class="fa fa-square font"></i>Provide advice to customers regarding different brands and their standing in the market</p>
        <p><i class="fa fa-square font"></i>Lead customers through the payment process to POS stations and / or processing their payments by cash or credit cards</p>
        <p><i class="fa fa-square font"></i>Ascertain that product warranties are handed over to customers at the time of purchase</p>
        <p><p><i class="fa fa-square font"></i>Ascertain that cleanliness and maintenance of showroom by coordinating custodial services</p>
        <p><i class="fa fa-square font"></i>Handle customer complaints by ensuring complete customer satisfaction at all levels</p>
        <p><i class="fa fa-square font"></i>Keeping up to date with special promotions and putting up displays.</p><br>

        <h4>Job Requirements</h4><br>

        <p><i class="fa fa-square font"></i>At least associate degree in business administrative.</p>
        <p><i class="fa fa-square font"></i>Must be highly passionate about communicating and interacting with people.</p>
        <p><i class="fa fa-square font"></i>Be able to communicate well in Khmer and English.</p>
        <p><i class="fa fa-square font"></i>Possess some basic selling skills and interest in ICT gadgets would be added advantage.</p>
        <p><i class="fa fa-square font"></i>Tidy, smart appearance and pleasant manner.</p>
        <p><i class="fa fa-square font"></i>Be respectful and be flexible.</p>
        <p><i class="fa fa-square font"></i>Positive attitude, hard working and honesty.</p><br>

        <h4>3. Sales Engineer (2 positions)</h4><br>

        <h4>Job Responsibilities</h4><br>

        <p><i class="fa fa-square font"></i>Perform sales and technical support in retail stores.</p>
        <p><i class="fa fa-square font"></i>Recommend new technologies of the company products to customers.</p>
        <p><i class="fa fa-square font"></i>Awareness of latest technology related to the product and business.</p>
        <p><i class="fa fa-square font"></i>Follow up customer’s need.</p><br>


        <h4>Job Requirements</h4> <br>
        <p><i class="fa fa-square font"></i>At least associate degree in Computer Science.</p>
        <p><i class="fa fa-square font"></i>At least 1year experience in IT industrial.</p>
        <p><i class="fa fa-square font"></i>Good command of written and spoken English language.</p>
        <p><i class="fa fa-square font"></i>Positive attitude, hard working and honesty.</p>
        <p><i class="fa fa-square font"></i>Tidy, smart appearance and pleasant manner.</p><br>


        <h4>4. Sales Representatives (2 Positions)</h4><br>

        <h4>Job Responsibilities</h4><br>

        <p><i class="fa fa-square font"></i>Meet and work with new whole seller and retailer on behalf of company</p>
        <p><i class="fa fa-square font"></i>Report daily sales achievement and daily target to sale supervisor.</p>
        <p><i class="fa fa-square font"></i>Prepare daily and weekly sales report to sale supervisor regarding sale problems, challenges in the market.</p>
        <p><i class="fa fa-square font"></i>Report to sales supervisor regarding to customer complaints.</p>
        <p><i class="fa fa-square font"></i>Survey customers’ behaviors and satisfaction to sales supervisor.</p>
        <p><i class="fa fa-square font"></i>Follow up customer’s need, deliver product to customers and confirm them on payment.</p>
        <p><i class="fa fa-square font"></i>Distribute marketing material to the market.</p><br>


        <h4>Job Requirements</h4><br>

        <p><i class="fa fa-square font"></i>At least graduated high school or associate degree (Sales and Marketing)</p>
        <p><i class="fa fa-square font"></i>Strong commitment in sale target with achieve monthly sales target.</p>
        <p><i class="fa fa-square font"></i>Good communication, presentation and problem solving skill.</p>
        <p><i class="fa fa-square font"></i>Able to work under pressure, hard working, honest, reliable and initiative.</p>
        <p><i class="fa fa-square font"></i>Have experiences in sales/marketing is advantage.</p>
        <p><i class="fa fa-square font"></i>Good command of written and spoken English language.</p>
        <p><i class="fa fa-square font"></i>Good interpersonal skills</p>

        <p style="text-align: center;">All candidates should submit the documents, CV, Cover Letter, stating <strong>expected salary</strong> to:</p><br>
        <p style="text-align: center;"><a href="mailto:recruitment@ione2u.com"><strong>recruitment@ione2u.com</strong></a></p><br>
        <p style="text-align: center;"><strong>Or</strong></p><br>
        <p style="text-align: center;"><strong>4<sup>th</sup> Floor, Unit 01, #25, Mao Tse Tung Blvd, Sangkat Boeung Keng Kang 1,</strong></p><br>
        <p style="text-align: center;"><strong>Khan Chamkar Morn, Phnom Penh, Kingdom of Cambodia.</strong></p><br>
        <p style="text-align: center;">Any enquiries, please contact Tel:<strong> 023 456 3333/015 939778</strong></p><br>
        <p style="text-align: center;">Deadline: 11<sup>th</sup>&nbsp;Dec 2018</p><br>
    </div><br>
    <div class="fb">
        <div class="container">
            <div class="row">
                <div class="col-md-12 outlets-fb"style="background-color: #1cbac8;">
                    <span style="margin-right:40px;"> GET IN TOUCH WITH IONE</span>
                    <div class="btn text-center" style=" border: 2px solid gray;" onmouseover="this.style.border='2px solid white'" onmouseout="this.style.border='2px solid gray'">
                        <a href="https://www.facebook.com/iOne2uStore/" target="_blank" style="text-decoration:none; color:white;">
                            <i class="fa fa-address-card" style="padding-right:10px;font-size:20px; color:white;"> </i><span>Contacts Us</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection