@extends('front-end.partials.layout')
@section('title', '- FAQs')
@section('active-page-contacts', 'sale-noti')
@section('link-bootstrap')
    <!-- Latest compiled and minified CSS -->
    <style>
        .font{
            font-size:7px;
            padding-right:10px;
        }

        .contact-fb{
            margin-top:18px;
            color:white;

        }

        .fb{
            background-color:#f9a200;;
            height:80px;
            text-align:center;
        }

        input[type=text], select {
            width: 100%;
            padding: 12px 20px;
            margin: 8px 0;
            display: inline-block;
            border: 1px solid #ccc;
            border-radius: 4px;
            box-sizing: border-box;
        }

        input[type=submit] {
            width: 100%;
            background-color: #4CAF50;
            color: white;
            padding: 14px 20px;
            margin: 8px 0;
            border: none;
            border-radius: 4px;
            cursor: pointer;
        }

        input[type=submit]:hover {
            background-color: #45a049;
        }

        #input-text {
            border-radius: 5px;
            background-color: #f2f2f2;
            padding: 20px;
        }

    </style>
@endsection

@section('content')
    <div class="container">
        <div id="input-text">
            <div class="row">
                <div class="col-md-3"><br><br>
                    <h2>Contact iOne</h2><br>
                    <p>If you can’t find the information you are looking for on our website or you require alternative support, please contact us by filling out the customer service form.</p><br>
                    <p>Our welcoming iOne staff is ready to assist you!</p>

                </div>
                <div class="col-md-9"><br><br>
                    <form>
                        <label for="mEmail">My Email</label>
                        <input type="text" id="mEmail" name="myEmail" placeholder="My Email(require)" required>

                        <label for="mPhone">Last Name</label>
                        <input type="text" id="mPhone" name="myPhone" placeholder="My Phone(require)" required><br><br>
                        <div class="form-group">
                            <textarea class="form-control" placeholder="My Inquiry" rows="12"></textarea>
                        </div>
                        <button type="button" class="btn btn-secondary">Secondary</button>
                    </form>
                </div>
            </div>
        </div>
    </div><br><br><br>
    <div class="fb">
        <div class="container">
            <div class="row">
                <div class="col-md-12 contact-fb"style="background-color: #f9a200; font-size:24px;">
                    <span style="margin-right:40px;"> 023 99 75 75 | 081 99 75 75 | 085 99 75 75</span>
                    <div class="btn text-center">
                        <a href="https://www.facebook.com/iOne2uStore/" target="_blank" style="text-decoration:none; color:white; font-size:24px;">
                            <i class="fa fa-address-card" style="padding-right:10px;font-size:30px; color:white;"> </i><span> customer.service@ione2u.com</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection