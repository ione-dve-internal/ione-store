<section style="background-color:whitesmoke;">
    <div class="container">
        <div class="sec-title" style="padding-top:20px;">
            <h3 class="m-text5 t-left">
                Recommended Products
                <p style="width: 250px; height: 10px; border-bottom: 5px solid #0dc6c3; position: relative;"></p>
            </h3>
        </div>
        <div class="row" id="section-recommended-product" style="box-shadow:10px 10px 1000px 1px rgb(234, 234, 234);">
        </div>
    </div>
</section>