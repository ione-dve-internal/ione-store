
    <section class="newproduct bgwhite p-t-45 p-b-105">
        <div class="container">
            <div class="sec-title p-b-60">
                <h3 class="m-text5 t-center">
                    Our Branches
                </h3>
            </div>

            <!-- Slide2 -->
            <div class="wrap-slick2"style="padding-bottom:100px;">
                <div class="slick2">
                    <div class="item-slick2 p-l-15 p-r-15">
                        <!-- Block2 -->
                        <div class="block2">
                            <div class="block2-img wrap-pic-w of-hidden pos-relative ">
                                <img src="{{asset('images/footer-logo/footer-logo-0.png')}}" alt="IMG-LOGO">
                            </div>
                        </div>
                    </div>

                    <div class="item-slick2 p-l-15 p-r-15">
                        <!-- Block2 -->
                        <div class="block2">
                            <div class="block2-img wrap-pic-w of-hidden pos-relative">
                                <img src="{{asset('images/footer-logo/footer-logo-1.png')}}" alt="IMG-LOGO">
                            </div>

                        </div>
                    </div>

                    <div class="item-slick2 p-l-15 p-r-15">
                        <!-- Block2 -->
                        <div class="block2">
                            <div class="block2-img wrap-pic-w of-hidden pos-relative ">
                                <img src="{{asset('images/footer-logo/footer-logo-3.png')}}" alt="IMG-LOGO">
                            </div>

                        </div>
                    </div>
                    <div class="item-slick2 p-l-15 p-r-15">
                        <!-- Block2 -->
                        <div class="block2">
                            <div class="block2-img wrap-pic-w of-hidden pos-relative ">
                                <img src="{{asset('images/footer-logo/footer-logo-4.png')}}" alt="IMG-LOGO">
                            </div>
                        </div>
                    </div>

                    <div class="item-slick2 p-l-15 p-r-15">
                        <!-- Block2 -->
                        <div class="block2">
                            <div class="block2-img wrap-pic-w of-hidden pos-relative">
                                <img src="{{asset('images/footer-logo/footer-logo-5.png')}}" alt="IMG-LOGO">
                            </div>
                        </div>
                    </div>

                    <div class="item-slick2 p-l-15 p-r-15">
                        <!-- Block2 -->
                        <div class="block2">
                            <div class="block2-img wrap-pic-w of-hidden pos-relative">
                                <img src="{{asset('images/footer-logo/footer-logo-6.png')}}" alt="IMG-LOGO">
                            </div>
                        </div>
                    </div>

                    <div class="item-slick2 p-l-15 p-r-15">
                        <!-- Block2 -->
                        <div class="block2">
                            <div class="block2-img wrap-pic-w of-hidden pos-relative ">
                                <img src="{{asset('images/footer-logo/footer-logo-7.png')}}" alt="IMG-LOGO">
                            </div>
                        </div>
                    </div>

                    <div class="item-slick2 p-l-15 p-r-15">
                        <!-- Block2 -->
                        <div class="block2">
                            <div class="block2-img wrap-pic-w of-hidden pos-relative ">
                                <img src="{{asset('images/footer-logo/footer-logo-8.png')}}" alt="IMG-LOGO">
                            </div>
                        </div>
                    </div>

                    <div class="item-slick2 p-l-15 p-r-15">
                        <!-- Block2 -->
                        <div class="block2">
                            <div class="block2-img wrap-pic-w of-hidden pos-relative ">
                                <img src="{{asset('images/footer-logo/footer-logo-9.png')}}" alt="IMG-LOGO">
                            </div>
                        </div>
                    </div>

                    <div class="item-slick2 p-l-15 p-r-15">
                        <!-- Block2 -->
                        <div class="block2">
                            <div class="block2-img wrap-pic-w of-hidden pos-relative ">
                                <img src="{{asset('images/footer-logo/footer-logo-10.png')}}" alt="IMG-LOGO">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>
