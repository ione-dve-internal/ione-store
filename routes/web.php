<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view(
        'front-end.partials.layout'
    );
});
Route::get('/', function () {
    return view('front-end.partials.section');
});

Route::get('products', function () {
    return view('front-end.pages.product.index');
});
Route::get('shoppings', function () {
    return view('front-end.pages.shopping.index');
});

Route::get('about', function () {
    return view('front-end.pages.about.index');
});
Route::get('outlets', function () {
    return view('front-end.pages.about.outlets');
});

Route::get('career', function () {
    return view('front-end.pages.about.career');
});

Route::get('product-hot', function () {
    return view('front-end.pages.product.show-hot');
});

Route::get('product-recommended', function () {
    return view('front-end.pages.product.show-recommended');
});

Route::get('faqs', function () {
    return view('front-end.pages.support.faqs');
});

Route::get('t&c', function () {
    return view('front-end.pages.support.t&c');
});
Route::get('contact', function () {
    return view('front-end.pages.contact.contacts');
});

